<?php
class UserModel extends Model
{

    public function create($userData)
    {
        $this->createTableUsers();

        //Проверка на наличие пользователя в таблице users
        $userExistQuery = "SELECT EXISTS (SELECT * FROM users WHERE email='{$userData['email']}')";
       
        if($this->database->query($userExistQuery)->fetch_row()[0]>0){
            //Если пользователь существует в системе
            return 0;
        }else{
            //Если пользователя нет в системе
            $createUserQuery = "INSERT INTO users SET ";

            foreach ($userData as $key => $value) {
                $createUserQuery.="`$key` = '$value',";
            };

            $this->database->query(substr($createUserQuery, 0, -1));
            return 1;
        }
    }

    public function getUserData($userID)
    {
        $query = "SELECT * FROM users WHERE id = $userID";
        $result = $this->database->query($query);
        return $result->fetch_assoc();
    }

    private function createTableUsers()
    {
        //Создание таблицы, если она не существует
        $createTableQuery = 'CREATE TABLE IF NOT EXISTS users 
        (
            id INT AUTO_INCREMENT PRIMARY KEY,
            email VARCHAR(30),
            password VARCHAR(50) NOT NULL,
            firstName VARCHAR(30),
            secondName VARCHAR(30),
            birthday DATE,
            phone VARCHAR(30),
            create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        );';

        if($this->database->query($createTableQuery)===true){
            //создание индекса на колонку email
            $createIndexQuery = "CREATE UNIQUE INDEX email ON users(email);";
            $this->database->query($createIndexQuery);
            return 0;
        }else{
            return $this->database->error;
        };
    }
}
?>