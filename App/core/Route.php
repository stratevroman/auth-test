<?php

/*
Класс-маршрутизатор для определения запрашиваемой страницы.
> цепляет классы контроллеров и моделей;
> создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/
class Route
{

	static function start()
	{
		// контроллер и действие по умолчанию
		$controllerName = 'Main';
		$actionName = 'index';
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		// получаем имя контроллера
		if ( !empty($routes[1]) )
		{	
			$controllerName = $routes[1];
		}
		
		// получаем имя экшена
		if ( !empty($routes[2]) )
		{
			$actionName = $routes[2];
		}

		// добавляем префиксы
		$modelName = $controllerName."Model";
		$controllerName = $controllerName."Controller";
		$actionName = $actionName."Action";

		/*
		echo "Model: $model_name <br>";
		echo "Controller: $controller_name <br>";
		echo "Action: $action_name <br>";
		*/

		// подцепляем файл с классом модели (файла модели может и не быть)

		$modelFile = ucfirst($modelName).'.php';
		$modelPath = "../App/models/".$modelFile;
		if(file_exists($modelPath))
		{
			include $modelPath;
		}

		// подцепляем файл с классом контроллера
		$controllerFile = ucfirst($controllerName).'.php';
		$controllerPath = "../App/controllers/".$controllerFile;
		if(file_exists($controllerPath))
		{
			include $controllerPath;
		}
		else
		{
			/*
			правильно было бы кинуть здесь исключение,
			но для упрощения сразу сделаем редирект на страницу 404
			*/
			Route::ErrorPage404();
		}
		
		// создаем контроллер
		$controller = new $controllerName;
		$action = $actionName;
		
		if(method_exists($controller, $action))
		{
			// вызываем действие контроллера
			$controller->$action();
		}
		else
		{
			// здесь также разумнее было бы кинуть исключение
			Route::ErrorPage404();
		}
	
	}

	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
        header('Location:'.$host.'notfound');
        return 0;
    }
    
}
