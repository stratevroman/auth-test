<?php
class Model
{
    public $database;

    public function __construct()
    {
        $this->createConnectionDB();
    }

    public function __destruct()
    {
        $this->closeConnectionDB();
    }

    private function createConnectionDB()
    {
        $host = getenv('DB_HOST'); // адрес сервера 
        $database = getenv('DB_NAME'); // имя базы данных
        $user = getenv('DB_USER'); // имя пользователя
        $password = getenv('DB_PASSWORD'); // пароль

        $this->database = new mysqli($host, $user, $password, $database);

        if (mysqli_connect_errno()) { 
            echo "Ошибка подключения к серверу MySQL. Код ошибки:".mysqli_connect_error(); 
            exit; 
        } 
    }

    private function closeConnectionDB()
    {
        $this->database->close(); 
    }

}
?>