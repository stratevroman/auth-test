<?php
class Controller
{
    public $model;
	public $view;
	
	function __construct()
	{
		$this->view = new View();
	}
	
	// действие (action), вызываемое по умолчанию
	function indexAction()
	{
		// todo	
	}

	function redirect($url)
	{
		header("Location: http://".$_SERVER['HTTP_HOST'].$url);
	}
}
?>