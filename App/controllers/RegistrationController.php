<?php

require_once('../App/models/UserModel.php');

class RegistrationController extends Controller
{

	public function __construct()
	{
		$this->view=new View();
		$this->model=new UserModel();
	}

    function indexAction()
	{	
		$data = array(
			'title'=>'Регистрация'
		);
		 
		$this->view->generate('registration_view.php', 'template_view.php',$data);
	}

	function createAction()
	{
		$userData=[];

		$errors=[];
		
		if(isset($_POST['firstName'])){
			$firstName = $this->validator('text',$_POST['firstName']);
			if($firstName){
				$userData['firstName']=$firstName;
			}else{
				$errors['firstName']='Введите имя';
			};
		};

		if(isset($_POST['secondName'])){
			$secondName = $this->validator('text',$_POST['secondName']);
			if($secondName){
				$userData['secondName']=$secondName;
			}else{
				$errors['secondName']='Введите фамилию';
			};
		};

		// if(isset($_POST['birthday'])){
		// 	$userData['birthday']=$this->validator('date',$_POST['birthday']);
		// };

		// if(isset($_POST['phone'])){
		// 	$userData['phone']=$this->validator('text',$_POST['phone']);
		// };

		if(isset($_POST['password'])){
			$password = $this->validator('text',$_POST['password']);
			if($password){
				$userData['password']=$password;
			}else{
				$errors['password']='Пароль меньше 8 символов';
			};
		};

		if(isset($_POST['email'])){
			$email = $this->validator('text',$_POST['email']);
			if($email){
				$userData['email']=$email;
			}else{
				$errors['email']='Не правильный email';
			};
		};
		//проверка на наличие ошибок
		if($errors){
			// $this->redirect("/dashboard");
		}else{
			if($this->model->create($userData)){
				$this->redirect("/dashboard");
			}else{
				$errors['userExist'] = 'Пользователь существует в системе';
				echo $errors;
				return $errors;
			}
		}
		
	}

	private function validator($type,$value)
	{
		switch($type)
		{
			case "text":
				$val = trim($value);
				$val = strip_tags($value);
				$val = htmlspecialchars($val);
				return $val;
			break;
			case "email":
				$val = strip_tags($value);
				$val = htmlspecialchars($val);
				if(filter_var($val, FILTER_VALIDATE_EMAIL)){
					return $val;
				}else{
					return 0;
				}
			break;
			case "date":
			break;
			case "password":
				if(count($value)<8){
					return 1;
				}else{
					return password_hash($value, PASSWORD_DEFAULT);
				}
			break;
			case "phone":
				
			break;
		}
	}
}
?>