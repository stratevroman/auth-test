<?php

require_once('../App/models/UserModel.php');

class DashboardController extends Controller
{
    public function __construct()
	{
		$this->view=new View();
		$this->model=new UserModel();
    }
    
    function indexAction()
	{	
        
		$data = array(
            'title'=>'Личный кабинет',
            'userData'=>$this->model->getUserData(5)
		);
		
        $this->view->generate('dashboard_view.php', 'template_view.php',$data);
        
	}
}
?>