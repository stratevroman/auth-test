<section class="mt-3">
    <div class="container">
        <div class="row text-center">
            <h1 class="col-md-12">
                <?=$data['title']?>
            </h1>
            <form class="col-md-4 offset-md-4">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введи email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                </div>

                <button type="submit" class="btn btn-primary">Войти</button>
            </form>
        </div>
    </div>
</section>