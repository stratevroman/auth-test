<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <title>
    <?php 
    if(isset($data['title']))
    {
        echo $data['title'];
    }else
    {
        echo 'Страница';
    };
    ?>
    </title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark" role="navigation">
        <div class="collapse navbar-collapse" id="navbarsDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                <a class="nav-link" href="/auth">Авторизация </a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/registration">Регистрация </a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<?php include '../App/views/'.$contentView; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
