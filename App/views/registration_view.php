<section class="mt-3">
    <div class="container">
        <div class="row text-center">
            <h1 class="col-md-12">
                <?=$data['title']?>
            </h1>
            <form class="col-md-4 offset-md-4 text-left" method="POST" action="/registration/create">
                <div class="form-group">
                    <label for="firstName">Имя</label>
                    <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Введите ваше имя">
                </div>
                <div class="form-group">
                    <label for="secondName">Фамилия</label>
                    <input type="text" class="form-control" name="secondName" id="secondName" placeholder="Введите вашу фамилию">
                </div>
                <div class="form-group">
                    <label for="birthday">Дата рождения</label>
                    <input type="text" class="form-control" name="birthday" id="birthday" placeholder="Введите дату рождения">
                </div>
                <div class="form-group">
                    <label for="phone">Номер телефона</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Номер телефона">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" aria-describedby="email" placeholder="Введи email">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Пароль">
                </div>
                <!-- <div class="form-group">
                    <label for="copyPassword">Подтверждение пароля</label>
                    <input type="password" class="form-control" id="copyPassword" placeholder="Подтвердите пароль">
                </div> -->
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                </div>
                
            </form>
        </div>
    </div>
</section>