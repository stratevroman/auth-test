<section class="mt-3">
    <div class="container">
        <h1><?=$data['title']?></h1>
        <?php 
        if(isset($data['userData'])){
            foreach ($data['userData'] as $field=>$value) {
                echo "<p><b>$field</b> - $value</p>";
            }
        }
        ?>
    </div>
</section>