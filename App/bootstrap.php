<?php 
require_once '../vendor/autoload.php';
//класс для работы с переменнами среды, хранятся в .env
use \Dotenv\Dotenv;
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

require_once 'core/Model.php';
require_once 'core/View.php';
require_once 'core/Controller.php';
require_once 'core/Route.php';
echo Route::start(); // запускаем маршрутизатор


?>